import React from "react";
import { ILoginModel } from "../models/ILoginModel";
import { ButtonComponent } from '@syncfusion/ej2-react-buttons';
import { TextBoxComponent, ChangedEventArgs } from '@syncfusion/ej2-react-inputs';
import { ToastComponent } from '@syncfusion/ej2-react-notifications';

interface Props {
}

export class LoginForm extends React.Component<Props, ILoginModel>{
    constructor(props: Props) {
        super(props);
        this.state = { username: "", password: "" };
        this.toastInstance = new ToastComponent({ title: "adasd", content: "asdasdasdasdasd" });
        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
    }

    public toastInstance: ToastComponent;

    public toastCreated(): void {
        this.toastInstance.show();
    }


    private onChangeUsername(arg?: ChangedEventArgs): void {
        if (arg != null) {
            this.setState({
                username: arg.value as string
            });
        }
    }

    private onChangePassword(arg?: ChangedEventArgs): void {
        if (arg != null) {
            this.setState({
                password: (arg.value as string)
            });
        }
    }

    private onSubmit(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();
        if (this.state.password === "p" && this.state.username === "u") {
            this.toastInstance.setProperties({ width: 400, title: "Sukces!", content: "Poprawnie zalogowano, trwa zmiana strony.", cssClass: 'e-toast-success', icon: 'e-success toast-icons' });
        }
        else {
            this.toastInstance.setProperties({ width: 400, cssClass: 'e-toast-danger', icon: 'e-error toast-icons', title: "Fail!", content: "Zakoduj sprawdzanie poprawności danych logowania! user: " + this.state.username + "  password:" + this.state.password });
        }
        this.toastInstance.show();
    }

    render() {
        return (
            <div className='control-pane'>
                <h2>Login</h2>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <TextBoxComponent placeholder="Username" cssClass="e-outline" floatLabelType="Auto" value={this.state.username} change={this.onChangeUsername} />
                    </div>
                    <ToastComponent ref={toast => this.toastInstance = toast!} />
                    <div className="form-group">
                        <TextBoxComponent placeholder="Password" cssClass="e-outline" floatLabelType="Auto" value={this.state.password} type="password" change={this.onChangePassword} />
                    </div>
                    <ButtonComponent type="submit" cssClass='e-success'>Login</ButtonComponent>
                </form>
            </div>
        )
    };
}