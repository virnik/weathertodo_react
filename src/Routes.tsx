import * as React from 'react'
import { Route, Switch, BrowserRouter } from 'react-router-dom'

import { LoginPage } from './pages/LoginPage'
import { ToDoPage } from './pages/ToDoPage'

export const Routes: React.SFC = () => (
    <BrowserRouter >
            <Switch>
                <Route exact path="/" component={LoginPage} />
                <Route exact path="/todo" component={ToDoPage} />
                <Route component={() => <div>Not Found</div>} />
            </Switch>
    </BrowserRouter >
)