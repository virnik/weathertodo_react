import React, { Component } from 'react';
import './App.css';
import { Routes } from './Routes';


class App extends Component<{}> {

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-2"></div>
          <div className="col-md-8">
            <div className="App">
              <article>
                <Routes />
              </article>
            </div>
          </div>
          <div className="col-md-2"></div>
        </div>
      </div>
    );
  }
}
export default App;
