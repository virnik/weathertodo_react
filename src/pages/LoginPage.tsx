import React, { FunctionComponent } from "react";
import { LoginForm } from "../components/LoginForm";

interface Props {
}

export const LoginPage: FunctionComponent<Props> = () => (
    <div className="container">
        <div className="row">
            <div className="col-md-2"></div>
            <div className="col-md-8">
                <div className="App">
                    <article>
                        <LoginForm />
                    </article>
                </div>
            </div>
            <div className="col-md-2"></div>
        </div>
    </div>
);