import React, { FunctionComponent } from "react";
import { NavBar } from "../components/NavBar";

interface Props {
}

export const ToDoPage: FunctionComponent<Props> = () => (
    <div className="container">
        <div className="row">
            <div className="col-md-12">
                <NavBar />
            </div>
        </div>
        <div className="row">
            <div className="col-md-2"></div>
            <div className="col-md-8">
                <div className="App">
                    <article>
                        ToDo Page
                    </article>
                </div>
            </div>
            <div className="col-md-2"></div>
        </div>
    </div>
);